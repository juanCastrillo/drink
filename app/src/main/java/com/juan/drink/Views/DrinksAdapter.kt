package com.juan.drink.Views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.juan.drink.R

class DrinksAdapter: RecyclerView.Adapter<DrinksAdapter.DrinksViewHolder>() {

    private var numberOfDrinks = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.drink, parent, false)
        return DrinksViewHolder(view)
    }

    fun updateNumberOfdrinks(nD:Int){
        numberOfDrinks = nD
        if(nD >= 1)
            notifyItemInserted(nD-1)
        else
            notifyDataSetChanged()
    }

    override fun getItemCount(): Int = numberOfDrinks


    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {

    }

    class DrinksViewHolder(v: View): RecyclerView.ViewHolder(v) {

    }
}