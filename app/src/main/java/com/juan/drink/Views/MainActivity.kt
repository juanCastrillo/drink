package com.juan.drink.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomappbar.BottomAppBar
import com.juan.drink.DrinkViewModel
import com.juan.drink.DrinkViewModelFactory
import com.juan.drink.MainApplication
import com.juan.drink.R
import com.juan.drink.services.DayChangeJob
import com.juan.drink.utils.NotificationUtils
import com.juan.drink.utils.toCustomString
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    private lateinit var drinksViewModel: DrinkViewModel
    private lateinit var drinksRecyclerView: RecyclerView
    private lateinit var drinksAdapter: DrinksAdapter

    private lateinit var dateTextView:TextView
    private lateinit var drinkCountTextView: TextView

    private lateinit var bottomAppBar: BottomAppBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drinksViewModel = ViewModelProviders.of(this,
            DrinkViewModelFactory(application, (application as MainApplication).drinkRepository)
        ).get(DrinkViewModel::class.java)

        drinkCountTextView = findViewById(R.id.drinkCount)
        dateTextView = findViewById(R.id.dateText)
        drinksViewModel.dateToday.observe(this, Observer {
            dateTextView.text = it.toCustomString()
        })


        drinksRecyclerView = findViewById(R.id.drinks_list)
        drinksAdapter = DrinksAdapter()
        drinksViewModel.numberOfDrinksToday.observe(this, Observer {
            drinksAdapter.updateNumberOfdrinks(it)
            drinkCountTextView.text = it.toString()
        })
        drinksRecyclerView.adapter = drinksAdapter
        drinksRecyclerView.layoutManager = GridLayoutManager(this, 5)

        bottomAppBar = findViewById(R.id.bottomAppBar)
        setSupportActionBar(bottomAppBar)

    }

    fun addDrink(view: View) {
        Log.d("MainActivity","adding drink")
        drinksViewModel.addDrink()
        drinksRecyclerView.smoothScrollToPosition(drinksAdapter.itemCount-1)
    }

    fun removeAllDrinks(view:View){
        drinksViewModel.removeAllDrinks()
    }

    fun sendNotification(view:View){
        NotificationUtils(this).sendDrinkNotification()
    }

    fun saveDay(view:View) {
        startService(Intent(this, DayChangeJob::class.java))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.history -> startActivity(Intent(this, HistoryActivity::class.java))
        }
        return true
    }

}
