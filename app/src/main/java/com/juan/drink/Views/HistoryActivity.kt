package com.juan.drink.Views

import android.os.Bundle
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juan.drink.DrinkViewModel
import com.juan.drink.DrinkViewModelFactory
import com.juan.drink.MainApplication
import com.juan.drink.R
import com.juan.drink.storage.entities.Drink
import com.juan.drink.utils.toCustomString
import javax.inject.Inject

class HistoryActivity: AppCompatActivity() {

    private lateinit var drinksHistoryRecyclerView:RecyclerView
    private lateinit var drinksAdapter: DrinkHistoryAdapter

    @Inject
    private lateinit var drinksViewModel: DrinkViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        drinksViewModel = ViewModelProviders.of(this,
            DrinkViewModelFactory(application, (application as MainApplication).drinkRepository)
        ).get(DrinkViewModel::class.java)
        drinksAdapter = DrinkHistoryAdapter()

        drinksViewModel.getAllDrinks().observe(this, Observer {
            drinksAdapter.updateHistory(it)
        })

        drinksHistoryRecyclerView = findViewById(R.id.drinks_history)
        drinksHistoryRecyclerView.adapter = drinksAdapter
        drinksHistoryRecyclerView.layoutManager = LinearLayoutManager(this)
        drinksHistoryRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

    }
}

class DrinkHistoryAdapter(): RecyclerView.Adapter<DrinkHistoryAdapter.DrinkHistoryViewHolder>() {

    var drinkHistoryList = listOf<Drink>()

    fun updateHistory(drinkHistoryList:List<Drink>){
        this.drinkHistoryList = drinkHistoryList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkHistoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.drink_row, parent, false)
        return DrinkHistoryViewHolder(view)
    }

    override fun getItemCount(): Int = drinkHistoryList.size

    override fun onBindViewHolder(holder: DrinkHistoryViewHolder, position: Int) {
        holder.drinkDate.text = drinkHistoryList[position].date.toCustomString()
        holder.drinkCount.text = drinkHistoryList[position].drinkNumber.toString()
    }

    class DrinkHistoryViewHolder(view: View):RecyclerView.ViewHolder(view) {
        var drinkDate:TextView
        var drinkCount:TextView
        init {
            drinkDate = view.findViewById(R.id.drinks_history_date)
            drinkCount = view.findViewById(R.id.drinks_history_count)
        }
    }

}