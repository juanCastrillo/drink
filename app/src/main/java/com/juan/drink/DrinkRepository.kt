package com.juan.drink

import androidx.room.Dao
import com.juan.drink.storage.daos.DrinkDao
import com.juan.drink.storage.entities.Drink
import org.threeten.bp.LocalDate
import java.util.*
import javax.inject.Inject

class DrinkRepository(private val drinkDao: DrinkDao) {

    fun getAllDrinks() = drinkDao.getAll()
    fun getAllDrinksOn(date: LocalDate) = drinkDao.getAllDrinksOn(date)

    fun saveDay(drink: Drink) {drinkDao.saveDay(drink)}

    fun pagingAllDrinks() = drinkDao.getAll()

}