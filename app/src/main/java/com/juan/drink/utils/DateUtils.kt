package com.juan.drink.utils

import android.util.Log
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import java.text.DateFormat
import java.text.SimpleDateFormat
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

class DateUtils {
    companion object {
        fun dayChangeTime():Long{
            val cal = Calendar.getInstance()
            cal.set(Calendar.HOUR_OF_DAY, 0)
            cal.set(Calendar.MINUTE, 0)
            return cal.timeInMillis + 24*60*60*1000
        }

        fun today() = LocalDate.now()

        fun isNightTime():Boolean {

            val nightTime = LocalTime.of(0,0)
            val dayTime = LocalTime.of(9,0)
            val nowTime = LocalTime.now()

            Log.d("isNightTime", "$dayTime > $nowTime > $nightTime")

            return nowTime in (nightTime )..(dayTime)
        }

        fun localDateToArchiveString(date:LocalDate):String = archiveFormatterLD.format(date)

        fun ArchiveStringtoLocalDate(stringDate:String):LocalDate = LocalDate.parse(stringDate, archiveFormatterLD)

        private val archiveFormatterLD = DateTimeFormatter.ofPattern("dd/MM/yyyy")
    }
}

private val customFormatter = DateTimeFormatter.ofPattern("eeee, dd LLLL")

fun LocalDate.toCustomString() = customFormatter.format(this)