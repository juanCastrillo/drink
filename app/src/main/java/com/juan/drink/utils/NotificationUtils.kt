package com.juan.drink.utils

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.juan.drink.Views.MainActivity
import com.juan.drink.R
import com.juan.drink.services.AddDrinkService

class NotificationUtils(val ct: Context) {

    val notificationManager = ct.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    /**
     * INITIALIZE all the notification channels
     */
    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(drinkChannel())
        }
    }

    fun sendDrinkNotification() {
        notificationManager.notify(DRINKNOTIFICATION_ID, drinkNotification())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun drinkChannel() = NotificationChannel(
        DRINKNOTIFICATION_CHANNEL_ID,
        DRINKNOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
    )

    private fun mainActivityIntent() = PendingIntent.getActivity(ct, 0, Intent(ct, MainActivity::class.java), 0)

    private fun drinkNotification() =
        NotificationCompat.Builder(ct, DRINKNOTIFICATION_CHANNEL_ID)
            .setColor(ContextCompat.getColor(ct, R.color.material_blue_grey_800))
            .setSmallIcon(R.drawable.glass_of_water_still)
            .setContentTitle(ct.getString(R.string.drinknotification_title))
            .setContentText(ct.getString(R.string.drinknotification_text))
            .setLargeIcon(BitmapFactory.decodeResource(ct.resources, R.drawable.glass_of_water_still))
            .setContentIntent(mainActivityIntent())
            .addAction(
                R.drawable.ic_add_black_24dp,
                ct.getString(R.string.drinknotification_add_drink_button_text),
                addDrinkButtonPendingIntent()
            )
            .setAutoCancel(true)
            .build()

    private fun addDrinkButtonPendingIntent() =
        PendingIntent.getService(ct, 0, Intent(ct, AddDrinkService::class.java), 0)

    companion object {

        val DRINKNOTIFICATION_CHANNEL_ID = "drink-channel"
        val DRINKNOTIFICATION_CHANNEL_NAME = "Need to drink"

        val DRINKNOTIFICATION_ID = 42

    }
}


