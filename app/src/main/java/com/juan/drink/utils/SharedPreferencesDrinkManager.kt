package com.juan.drink.utils

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.juan.drink.R
import com.juan.drink.utils.DateUtils.Companion.ArchiveStringtoLocalDate
import com.juan.drink.utils.DateUtils.Companion.localDateToArchiveString
import com.juan.drink.utils.DateUtils.Companion.today
import org.threeten.bp.LocalDate

class SharedPreferencesDrinkManager(ct:Context) {

    val sp = PreferenceManager.getDefaultSharedPreferences(ct)

    fun getNumberOfDrinks() = sp.getInt(NUMBER_DRINKS_KEY, 0)
    fun getDateOfDrinks() = sp.getString(DATE_DRINK_KEY, null)

    fun addDrink(){

        //guardo el valor anterior
        val previous = getNumberOfDrinks()

        //añadir una bebida más a mis preferencias
        sp.edit().putInt(NUMBER_DRINKS_KEY, previous+1).apply() //editar la preferencia y añadir ++ al valor anterior
    }

    fun removeDrink(){

        //guardo el valor anterior
        val previous = getNumberOfDrinks()

        //reducir una bebida a mis preferencias
        sp.edit().putInt(NUMBER_DRINKS_KEY, previous-1).apply() //editar la preferencia y restar uno al valor anterior
    }

    fun clearDrinkCount(){
        sp.edit().putInt(NUMBER_DRINKS_KEY, 0).apply() //editar la preferencia y añadir ++ al valor anterior
    }

    fun getDrinkDate(): LocalDate {
        val date = getDateOfDrinks()
        return if(date == null) {
            updateDrinkDate()
            today()
        } else ArchiveStringtoLocalDate(date)
    }

    fun updateDrinkDate() {
        sp.edit().putString(DATE_DRINK_KEY, localDateToArchiveString(today())).apply()
    }

    companion object {
        val NUMBER_DRINKS_KEY = "number-drinks"
        val DATE_DRINK_KEY = "date-drinks"
    }
}