package com.juan.drink

import android.app.Application
import android.content.SharedPreferences
import android.os.AsyncTask
import android.preference.PreferenceManager
import android.util.Log
import androidx.lifecycle.*
import com.juan.drink.storage.entities.Drink
import com.juan.drink.utils.DateUtils
import com.juan.drink.utils.DateUtils.Companion.today
import com.juan.drink.utils.SharedPreferencesDrinkManager
import com.juan.drink.utils.SharedPreferencesDrinkManager.Companion.DATE_DRINK_KEY
import com.juan.drink.utils.SharedPreferencesDrinkManager.Companion.NUMBER_DRINKS_KEY
import org.threeten.bp.LocalDate
import java.util.*
import javax.inject.Inject

class DrinkViewModel(application: Application, @Inject val drinkRepository: DrinkRepository): AndroidViewModel(application), SharedPreferences.OnSharedPreferenceChangeListener {

    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)
    val numberOfDrinksToday: MutableLiveData<Int> = MutableLiveData()
    val dateToday:MutableLiveData<LocalDate> = MutableLiveData()

    private val context = getApplication<MainApplication>()
    val sharedPreferencesDrinkManager: SharedPreferencesDrinkManager

    init{
        sharedPreferencesDrinkManager = SharedPreferencesDrinkManager(context)
        numberOfDrinksToday.postValue(sharedPreferencesDrinkManager.getNumberOfDrinks())
        sharedPreferences.registerOnSharedPreferenceChangeListener(this)
        //TODO check if date saved on shared preferences is the same as today and if not save in db and reset day
        numberOfDrinksToday.postValue(sharedPreferencesDrinkManager.getNumberOfDrinks())
        dateToday.postValue(today())
        val previousDate = sharedPreferencesDrinkManager.getDrinkDate()
        Log.d("DrinkViewModel", "previousDate: $previousDate == todayDate: ${today()}")

        AsyncTask.execute {
            if (previousDate != today()) {
                sharedPreferencesDrinkManager.updateDrinkDate()
                drinkRepository.saveDay(Drink(null, sharedPreferencesDrinkManager.getNumberOfDrinks(), previousDate))
                sharedPreferencesDrinkManager.clearDrinkCount()
            }
        }
    }

    fun getAllDrinks() = drinkRepository.getAllDrinks()

//    fun getAllDrinksPaged(): LiveData<PagedList<Drink>> = LivePagedListBuilder(
//        drinkRepository.pagingAllDrinks(), 50).build()

    fun getAllDrinksOn(date: LocalDate) = drinkRepository.getAllDrinksOn(date)

    fun addDrink() {
        sharedPreferencesDrinkManager.addDrink()
    }

    fun removeAllDrinks(){
        sharedPreferencesDrinkManager.clearDrinkCount()
    }

    fun removeADrink() {
        sharedPreferencesDrinkManager.clearDrinkCount()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        Log.d("DrinkViewModel", "preferencechanged: $key")
        when(key){
            NUMBER_DRINKS_KEY -> numberOfDrinksToday.value = sharedPreferencesDrinkManager.getNumberOfDrinks()
            DATE_DRINK_KEY -> dateToday.value = sharedPreferencesDrinkManager.getDrinkDate()
        }
    }
}

class DrinkViewModelFactory(val application: Application, val drinkRepository: DrinkRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DrinkViewModel(application, drinkRepository) as T
    }

}