package com.juan.drink.Services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class ChangeDayBroadcast: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        context.startService(Intent(context, DayChangeJob::class.java))
    }
}