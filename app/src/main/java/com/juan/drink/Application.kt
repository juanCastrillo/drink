package com.juan.drink

import android.app.Application
import androidx.room.Room
import com.jakewharton.threetenabp.AndroidThreeTen
import com.juan.drink.services.JobScheduling
import com.juan.drink.storage.databases.DrinksDatabase

open class MainApplication: Application() {

    lateinit var drinkRepository: DrinkRepository
    private lateinit var drinksDatabase: DrinksDatabase

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this);
        JobScheduling(this).sheduleAllJobs()

        drinksDatabase = Room.databaseBuilder(this, DrinksDatabase::class.java, "drinks_database").build()
        drinkRepository = DrinkRepository(drinksDatabase.drinkDao())
    }
}