package com.juan.drink.storage.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.juan.drink.storage.daos.DrinkDao
import com.juan.drink.storage.entities.DateConverters
import com.juan.drink.storage.entities.Drink

@Database(entities = [Drink::class], version = 1)
@TypeConverters(DateConverters::class)
abstract class DrinksDatabase: RoomDatabase(){
    abstract fun drinkDao():DrinkDao
}