package com.juan.drink.storage.daos

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.juan.drink.storage.entities.Drink
import org.threeten.bp.LocalDate
import java.util.*

@Dao
interface DrinkDao {

    @Query("SELECT * FROM drinks")
    fun getAll(): LiveData<List<Drink>>//DataSource.Factory<Int, Drink>

    @Query("SELECT * FROM drinks")
    fun getAllPaged(): DataSource.Factory<Int, Drink>

    @Query("SELECT * FROM drinks WHERE date = :date")//WHERE date = :date")
    fun getAllDrinksOn(date: LocalDate):LiveData<List<Drink>>//date: LocalDate):LiveData<List<Drink>>

    @Insert(onConflict = REPLACE)
    fun saveDay(drink:Drink)
}