package com.juan.drink.storage.entities

import androidx.room.*
import com.juan.drink.utils.DateUtils

import com.juan.drink.utils.DateUtils.Companion.ArchiveStringtoLocalDate
import com.juan.drink.utils.DateUtils.Companion.localDateToArchiveString
import org.threeten.bp.LocalDate
import java.util.*

@Entity(tableName = "drinks")
data class Drink(
    @PrimaryKey
    @ColumnInfo(name = "drinks_id")
    val id:Long?,
    @ColumnInfo(name = "drink_number")
    var drinkNumber:Int,
    var date: LocalDate
)

//TODO finish converters, add info into google docs (room)
//TODO add the converters to the database
//TODO in the DayChange job add the correct date when saving
class DateConverters {
    companion object {
        @JvmStatic
        @TypeConverter
        fun DateToStringConverter(date:LocalDate):String = localDateToArchiveString(date)

        @JvmStatic
        @TypeConverter
        fun StringToDateConverter(dateString:String):LocalDate = ArchiveStringtoLocalDate(dateString)
    }
}

