package com.juan.drink.Dependencies

import com.juan.drink.Views.MainActivity
import dagger.Component

@Component(modules = [DrinkModule::class])
interface DrinkComponent {
    fun inject(app:MainActivity)
}