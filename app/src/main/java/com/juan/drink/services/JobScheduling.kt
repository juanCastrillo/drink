package com.juan.drink.services

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.firebase.jobdispatcher.*

class JobScheduling(val ct:Context) {

    fun sheduleAllJobs(){
        startNotificationSchedule()
        registerChangeDayBroadcast()
    }

    private fun registerChangeDayBroadcast() {
        val intentFilter = IntentFilter().apply {
            addAction(Intent.ACTION_DATE_CHANGED)
        }
        ct.registerReceiver(ChangeDayBroadcast(), intentFilter)
    }

    fun startNotificationSchedule(){

        val gdp = GooglePlayDriver(ct)
        val fdispatcher = FirebaseJobDispatcher(gdp)

        val notificationJob = fdispatcher.newJobBuilder()
            .setService(SendNotification::class.java)
            .setTrigger(triggerNotificationSend)
            .setRecurring(true)
            .setTag(SEND_NOTIFICATION_JOB_TAG)
            .setReplaceCurrent(true)
            .setLifetime(Lifetime.FOREVER)
            //.addConstraint()

        fdispatcher.schedule(notificationJob.build())
    }

    /*fun updateDayChangeDrinkCount(){
        val manager = ct.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val pendingUpdate = PendingIntent.getService(ct, 0, Intent(ct, DayChangeJob::class.java), 0)
        manager.setRepeating(AlarmManager.RTC, dayChangeTime(), AlarmManager.INTERVAL_DAY, pendingUpdate)
    }*/

    companion object {

        val DAY_CHANGE_WINDOW_START = 12*3600
        val DAY_CHANGE_WINDOW_END = 12*3600

        val DRINK_NOTIFICATION_SEND_START = 60*60
        val DRINK_NOTIFICATION_SEND_END = 60*60

        val SEND_NOTIFICATION_JOB_TAG = "send-notification-job"

        val triggerNotificationSend = Trigger.executionWindow(DRINK_NOTIFICATION_SEND_START, DRINK_NOTIFICATION_SEND_START + DRINK_NOTIFICATION_SEND_END)
        val triggerUpdateDay = Trigger.executionWindow(DAY_CHANGE_WINDOW_START, DAY_CHANGE_WINDOW_START + DAY_CHANGE_WINDOW_END)
    }
}