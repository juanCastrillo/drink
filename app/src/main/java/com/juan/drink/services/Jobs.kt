package com.juan.drink.services

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import com.juan.drink.MainApplication
import com.juan.drink.storage.entities.Drink
import com.juan.drink.utils.DateUtils.Companion.isNightTime
import com.juan.drink.utils.DateUtils.Companion.today
import com.juan.drink.utils.NotificationUtils
import com.juan.drink.utils.SharedPreferencesDrinkManager

class DayChangeJob: IntentService("DayChangeJob") {
    override fun onHandleIntent(intent: Intent?) {
        Log.d("DayChangeJob", "changingDayxd")
        val spdm = SharedPreferencesDrinkManager(this)
        (application as MainApplication).drinkRepository.saveDay(Drink(null, spdm.getNumberOfDrinks(), today()))
        spdm.clearDrinkCount()
        spdm.updateDrinkDate()
    }
}

class SendNotification: JobService() {
    override fun onStopJob(job: JobParameters?): Boolean = true

    override fun onStartJob(job: JobParameters?): Boolean {
        if(isNightTime())
        NotificationUtils(this).sendDrinkNotification()
        return false
    }

}