package com.juan.drink.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class ChangeDayBroadcast: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {

        if(intent.action == Intent.ACTION_DATE_CHANGED) {
            Log.d("ChangeDayBroadcast", "ha cambiado la fecha")
            context.startService(Intent(context, DayChangeJob::class.java))
//        }else if(intent.action == Intent.ACTION_TIME_CHANGED){
//            Log.d("ChangeDayBroadcast", "ha cambiado algo")
        }
    }
}