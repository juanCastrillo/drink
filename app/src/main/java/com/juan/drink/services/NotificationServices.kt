package com.juan.drink.services

import android.app.IntentService
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.util.Log
import com.juan.drink.utils.SharedPreferencesDrinkManager
import com.juan.drink.utils.NotificationUtils.Companion.DRINKNOTIFICATION_ID

class AddDrinkService: IntentService("AddDrinkService") {

    override fun onHandleIntent(intent: Intent?) {

        SharedPreferencesDrinkManager(this).addDrink()

        try {
            val nm = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nm.cancel(DRINKNOTIFICATION_ID)
        }catch (e:Exception){
            Log.d("AddDrinkService", e.toString())}
    }
}