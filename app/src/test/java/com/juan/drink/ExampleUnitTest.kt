package com.juan.drink

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import com.juan.drink.utils.DateUtils.Companion.isNightTime
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(RobolectricTestRunner::class)
class JobTesting {

    val context = ApplicationProvider.getApplicationContext<Application>()

    @Test
    fun dayChangeTimeTest() {
        val expected = false
        val isNight = isNightTime()
        assertEquals(expected, isNight)
    }
}
