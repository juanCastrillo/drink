package com.juan.drink

import android.app.Application
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import com.juan.drink.utils.SharedPreferencesDrinkManager
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

/**
 * test all SharedPreferencesDrinkManager methods
 */
@RunWith(RobolectricTestRunner::class)
class SharedPreferencesDrinkManagerTests {

    val context = ApplicationProvider.getApplicationContext<Application>()

    @Test
    fun addDrinkTest() {
        val expected = 1

        //We add 1 drink to out preferences
        SharedPreferencesDrinkManager.addDrink(context)

        //get the number of drinks after adding it
        val numberDrinks = SharedPreferencesDrinkManager.getNumberOfDrinks(context)
        assertThat(expected, `is`(numberDrinks))
    }

    /**
     * test to remove a drink from shared preferences
     */
    @Test
    fun removeDrinkTest(){

        //we add 1 drink to our mocked shared preferences
        addDrinkTest()

        //remove 1 drink
        SharedPreferencesDrinkManager.removeDrink(context)

        //we expect to have 0 as we got rid of 1 drink
        val expectedNumberOfDrinks = 0

        //get the number of drinks
        val receivedNumberOfDrinks = SharedPreferencesDrinkManager.getNumberOfDrinks(context)

        assertThat(expectedNumberOfDrinks, `is`(receivedNumberOfDrinks))
    }

    /**
     * test to remove all drinks from shared preferences
     */
    @Test
    fun removeAllDrinksTest(){

        //we add 6 drinks to our mocked shared preferences
        for (i in 0..5) SharedPreferencesDrinkManager.addDrink(context)

        //remove all drinks
        SharedPreferencesDrinkManager.clearDrinkCount(context)

        //we expect to have 0 as we got rid of 1 drink
        val expectedNumberOfDrinks = 0

        //get the number of drinks
        val receivedNumberOfDrinks = SharedPreferencesDrinkManager.getNumberOfDrinks(context)

        assertThat(expectedNumberOfDrinks, `is`(receivedNumberOfDrinks))
    }
}